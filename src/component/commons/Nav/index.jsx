import { useHistory } from "react-router-dom";
import ROUTES from "../../../configs/routes";
import "./Navbar.scss";

const Nav = () => {
  const history = useHistory();
  const Submit = () =>{
    localStorage.removeItem("token")
    history.push(ROUTES.ROOT)
  }
    return(
  <div>
		<nav className="navbar navbar-expand-md navbar-dark shadow fixed-top navbar-custom px-5">
      <a className="navbar-brand" href="/home">Rock Paper Scissors</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#_navbar" aria-controls="_navbar"
      aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="_navbar">
      <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger" href="/dashboard">
              Dashboard
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger" href="/register">Register</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger" href="/login">Log in</a>
          </li>
          <li className="nav-item">
            <a className="nav-link js-scroll-trigger" onClick={Submit}>Log Out</a>
          </li>               
      </ul>
    </div>
    </nav>
	</div>
  )
}

export default Nav;