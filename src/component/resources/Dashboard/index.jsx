import "./Dashboard.scss";

const Dashboard = () => {
    return(
      <div>
        <main className="dashboard p-5">
        <div className="container p-3">
          <h3>List Of User</h3>
          <div class="table-responsive">
            <button className="btn btn-secondary btn-page" id="prev-user">Prev</button>
            <button className="btn btn-secondary btn-page" id="next-user" value='1'>Next</button>
            <table className="table table-bordered table-dark">
              <thead>
                <tr>
                  <th scope="col">Username</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Role</th>
                  <th scope="col">Gender</th>
                </tr>
              </thead>
              <tbody id="list-user"></tbody>
            </table>
          </div>

          <h3>Game History</h3>
          <div class="table-responsive">
            <button className="btn btn-secondary btn-page" id="prev-history">Prev</button>
            <button className="btn btn-secondary btn-page" id="next-history">Next</button>
            <table className="table table-bordered table-dark">
              <thead>
                <tr>
                  <th scope="col">Player 1</th>
                  <th scope="col">Picked</th>
                  <th scope="col">Player 2</th>
                  <th scope="col">Picked</th>
                  <th scope="col">Result</th>
                </tr>
              </thead>
              <tbody id="list-history"></tbody>
            </table>
          </div>
					
        </div>
      </main>
    </div>

    )
}
export default Dashboard;