import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import RegisterService from "../../../services/Register/serviceRegister";
import ROUTES from "../../../configs/routes"
import "./Register.scss";

const Register = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory()
  
  const Submit = ()=>{
    RegisterService(username, email, password)
    .then(
      (data)=>{
        localStorage.setItem("token",data.token)
        history.push(ROUTES.LOGIN)
      }
    )
    .catch(
      (e)=>{console.log(e)}
    )
  }
  return(
    <div className="register-box">
     <h2>Register</h2>
     <div>
       <div className="user-box">
          <input type="text" name="" required="" onChange={(e)=>setUsername(e.target.value)}/>
          <label>Username</label>
       </div>
       <div className="user-box">
         <input type="text" name="" required="" onChange={(e)=>setEmail(e.target.value)}/>
         <label>Email</label>
       </div>
       <div className="user-box">
         <input type="password" name="" required="" onChange={(e)=>setPassword(e.target.value)}/>
         <label>Password</label>
       </div>
       <button type="button" className="btn-lg" onClick={Submit}>
        Register
       </button>
     </div>
    </div>
  )
}
export default Register;