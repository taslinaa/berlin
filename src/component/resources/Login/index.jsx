import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./Login.scss";
import LoginService from "../../../services/Login/serviceLogin";
import ROUTES from "../../../configs/routes"

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory()
  
  const Submit = ()=>{
    LoginService(username, password)
    .then(
      (data)=>{
        localStorage.setItem("token",data.token)
        history.push(ROUTES.DASHBOARD)
      }
    )
    .catch(
      (e)=>{console.log(e)}
    )
  }
  return(
      <div className="login-box">
      <h2>Login</h2>
      <div>
        <div className="user-box">
          <input type="text" name="" required="" onChange={(e)=>setUsername(e.target.value)}/>
          <label>Username</label>
        </div>
        <div className="user-box">
          <input type="password" name="" required="" onChange={(e)=>setPassword(e.target.value)}/>
          <label>Password</label>
        </div>
        <button type="button" className="btn-lgn" onClick={Submit}>
          Login
        </button>
      </div>
    </div>
  )
}
export default Login;