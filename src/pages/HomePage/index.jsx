import React from "react";
import Nav from "../../component/commons/Nav";
import "./HomePage.scss";


const HomePage = () => {
    return(
    <>
    <Nav />
      <div class="jumbotron">
        <div class="container text-center jumbo-center">
          <h1>TRADITIONAL GAME PLAY</h1>
          <a href="#">
            <button class="btn btn-primary-custom px-5 mt-4" id="play">PLAY</button>
          </a>
        </div>
      </div>
    </>
    )
}
export default HomePage;