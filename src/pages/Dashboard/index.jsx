import Nav from "../../component/commons/Nav"
import Dashboard from "../../component/resources/Dashboard"

const DashboardPage = () => {
    
    return(
        <div>
            <Nav />
            <Dashboard />
        </div>

    )

    
}
export default DashboardPage;