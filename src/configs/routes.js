const ROUTES = {
    ROOT: "/",
    LOGIN: "/login",
    SIGNUP: "/register",
    DASHBOARD: "/dashboard",
    NOTFOUND: "/404",
  };
  
  export default ROUTES;