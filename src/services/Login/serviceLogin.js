import axios from "axios";
 
const LoginService = (username, password) => 
    new Promise (
      (resolve, reject) => {
          axios.post(`${process.env.REACT_APP_API_URL}/user/login`,{username, password})
          .then(
              (data)=> resolve(data.data)
              
          )
          .catch(
              (e) => reject(e) 
          )
        }

    )

export default LoginService;