import axios from "axios";

const RegisterService = (username, email, password) =>
    new Promise(
        (resolve, reject) => {
            axios.post(`${process.env.REACT_APP_API_URL}/user/register`,{username, email, password})
            .then(
                (data)=> resolve(data.data)
            )
            .catch(
                (e)=> reject(e)
            )
        }
    )
export default RegisterService;